import { RouterModule, Routes } from '@angular/router';
import { HeroesComponent } from './components/heroes/heroes.component';
import { HeroeComponent } from './components/heroe/heroe.component';
import { AboutComponent } from './components/about/about.component';
import { HomeComponent} from './components/home/home.component';
import { HeroeFindComponent } from './components/heroe-find/heroe-find.component';

export const ROUTES: Routes = [
    {path: 'home', component: HomeComponent},
    {path: 'heroes', component: HeroesComponent},
    {path: 'heroe/:nombre', component: HeroeComponent},
    {path: 'heroe-find/:busqueda', component: HeroeFindComponent},
    {path: 'about', component: AboutComponent},
    {path: '**',  pathMatch: 'full', redirectTo: 'home'}
];

export const APP_ROUTING = RouterModule.forRoot(ROUTES, );

