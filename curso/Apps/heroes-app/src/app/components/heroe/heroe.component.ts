import { Component, Input, OnInit } from '@angular/core';
import {HeroService} from '../../servicios/heroes.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-heroe',
  templateUrl: './heroe.component.html',
  styleUrls: ['./heroe.component.css']
})
export class HeroeComponent implements OnInit {

  heroe:any;
    
  

  constructor(private _heroeService:HeroService,
              private _activate:ActivatedRoute) { 
                _activate.params.subscribe(param => {
                  console.log(param);
                  let nombre = param['nombre'];
                  this.heroe = _heroeService.getHeroe(nombre);
                });
              }

  ngOnInit(): void {
  }

  

}
