import { Component, OnInit } from '@angular/core';
import {HeroService} from '../../servicios/heroes.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-heroes',
  templateUrl: './heroes.component.html',
  styleUrls: ['./heroes.component.css']
})
export class HeroesComponent implements OnInit {

  heroes:any[] = [];
  
  constructor(private _heroesService:HeroService,
              private _router:Router) {}

  ngOnInit(): void {
    this.heroes = this._heroesService.getHeroes();
  }

  onDetalles(nombre){   
    console.log("heroes: ", {nombre}) 
    this._router.navigate(['/heroe',nombre]);
  }



}
