import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';
import {HeroService} from '../../servicios/heroes.service';

@Component({
  selector: 'app-heroe-find',
  templateUrl: './heroe-find.component.html',
  styleUrls: ['./heroe-find.component.css']
})
export class HeroeFindComponent implements OnInit {

  heroes: any[];
  
    
  constructor(private route: ActivatedRoute,
              private _heroService:HeroService,
              private sanitizer:DomSanitizer) { 
    const busqueda = route.params.subscribe(
      params=> {
        this.pintarResultado(params);
        
      }
    );      
  }

  pintarResultado(params){
    
    let myH = [];
    let res = "";
    const miB = params["busqueda"];    
    const heroes = this._heroService.getHeroesFind(miB);      
    let regex = new RegExp(`(${miB})`, "ig");  
    for (let heroe of heroes){
      res = heroe.bio;
      if(miB !== ""){        
        res = res.replace(regex, "<span class = 'marcado'>$1</span>")                     
      }
      heroe["bio2"] = this.sanitizer.bypassSecurityTrustHtml(res); 
      myH.push(heroe);
    }    
    this.heroes = myH;
  }
  ngOnInit(): void {
  }

}
